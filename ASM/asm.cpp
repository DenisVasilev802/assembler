#include <iostream>
#include <stdio.h>
void main()
{
    const int N = 10;
    char* st1;
    char*  st2;
    char* res;
    __asm
    {
                                         // выделение памяти и считывание первой строки
        mov     eax, N                  // положили в еах размер строки
        inc     eax                     // увеличили размер на 1 под 0 символ
        push    eax
        call    dword ptr malloc        // выделяем память под первую строку
        add     esp, 4                  // чистим стек
        mov     st1, eax                
        cmp     eax, 0
        je      FIN                     // если памяти не хватило выход, в случае нехватки памяти malloc возвращает 0     
                
        push    eax                     // положили в стек адрес первой строки
        call    dword ptr gets          // считали строку 
        add     esp,4                   // убрали из стека адрес первой строки
 
        mov     ebx,st1                 // запомнили адрес первой строки
                                         // выделение памяти и считывание второй строки
        mov     eax, N                  // положили в еах размер строки
        inc     eax                     // увеличили размер на 1 под 0 символ
        push    eax
        call    dword ptr malloc        // выделяем память под вторую строку
        add     esp, 4                  // чистим стек
        mov     st2, eax                
        cmp     eax, 0
        je      FIN                     // если памяти не хватило выход, в случае нехватки памяти malloc возвращает 0 
 
        push    eax                     // положили в стек адрес второй строки
        call    dword ptr gets          // считали строку 
        add     esp,4                   // убрали из стека адрес второй строки
        
        mov     ecx,st2
        
                                       // подсчет символов
        xor     esi,esi
CYCLE_1:
        cmp     ebx,0       
            mov     dl,[eax][esi]
        cmp     dl,0
            je NEXT
 
            
FIN :
 
    }
    system ("pause");
}
